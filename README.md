# Choubun 超文

A library for generating HTML from Python using context managers.

> (this is not recommended as a real way of generating HTML!)

— [@contextlib.contextmanager](https://docs.python.org/3.5/library/contextlib.html#contextlib.contextmanager)
